package com.quinyx.innovation.iou.service;

import com.quinyx.innovation.iou.model.User;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author lukasz
 */
public class UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    final Datastore datastore;

    public UserService(Datastore datastore) {
        this.datastore = datastore;
    }

    // returns a list of all users
    public List<User> getAllUsers() {
        logger.info("Returning all users");
        Query<User> query = datastore.createQuery(User.class);
        List<User> users = query.asList();
        return users;
    }

    // returns a single user by id
    public User getUser(String email) {
        logger.info("Returning user by email (" + email + ")");
        User user = datastore.createQuery(User.class)
                .field("email").equal(email)
                .get();
        return user;
    }

    // creates a new user
    public User createUser(String email) {
        logger.info("Creating user by email (" + email + ")");
        User user = new User(email);
        datastore.save(user);
        return user;
    }

}
