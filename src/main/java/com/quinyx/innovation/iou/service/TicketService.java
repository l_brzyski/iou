package com.quinyx.innovation.iou.service;

import com.quinyx.innovation.iou.model.Ticket;
import com.quinyx.innovation.iou.model.User;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author lukasz
 */
public class TicketService {

    Logger logger = LoggerFactory.getLogger(TicketService.class);

    final Datastore datastore;

    public TicketService(Datastore datastore) {
        this.datastore = datastore;
    }

    // returns a list of all tickets
    public List<Ticket> getTickets(User from, User to) {
        logger.info("Returning tickets from {} to {}", from, to);
        Query<Ticket> query = datastore.createQuery(Ticket.class).filter("active", true);
        if (from != null){
            query.filter("from", from);
        }
        if (to != null){
            query.filter("to", to);
        }
        List<Ticket> tickets = query.asList();
        return tickets;
    }

    // returns a single ticket by id
    public Ticket getTicket(String id) {
        logger.info("Returning ticket by id (" + id + ")");
        Ticket ticket = datastore.createQuery(Ticket.class)
                .field("id").equal(new ObjectId(id)).get();

        return ticket;
    }

    // creates a new ticket
    public Ticket createTicket(User from, User to, String gift, String note) {
        Ticket ticket = new Ticket(from, to, gift, note);
        logger.info("Creating ticket {}", ticket);
        datastore.save(ticket);
        return ticket;
    }
    // inactivates a ticket
    public Ticket inactivateTicket(String id) {
        logger.info("Updating ticket {}", id);
        Query<Ticket> inactivatedTicket = datastore.createQuery(Ticket.class).field("id").equal(new ObjectId(id));
        UpdateOperations<Ticket> inactivateOperations = datastore.createUpdateOperations(Ticket.class).set("active", false);
        UpdateResults results = datastore.update(inactivatedTicket, inactivateOperations);

        return datastore.createQuery(Ticket.class).field("id").equal(new ObjectId(id)).get();
    }

}
