package com.quinyx.innovation.iou;

/**
 * @author lukasz
 */

import com.mongodb.MongoClient;
import com.quinyx.innovation.iou.controller.TicketController;
import com.quinyx.innovation.iou.controller.UserController;
import com.quinyx.innovation.iou.service.TicketService;
import com.quinyx.innovation.iou.service.UserService;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

public class IOUApplication {

    public static void main(String[] args) {
        Datastore datastore = initDatastore();

        UserService userService = new UserService(datastore);
        TicketService ticketService = new TicketService(datastore);
        new UserController(userService);
        new TicketController(ticketService, userService);
    }

    private static Datastore initDatastore() {
        final Morphia morphia = new Morphia();
        morphia.mapPackage("com.quinyx.innovation.iou.model");
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        Datastore datastore = morphia.createDatastore(mongoClient, "iou");
        datastore.ensureIndexes();

        return datastore;
    }
}
