package com.quinyx.innovation.iou.model;

import org.mongodb.morphia.annotations.*;

/**
 * @author lukasz
 */
@Entity("users")
@Indexes(
        @Index(value = "email", fields = @Field("email"))
)
public class User {
    @Id
    public String email;

    public User() {}

    public User(String email) {
        this.email = email;
    }
}
