package com.quinyx.innovation.iou.model;

import org.mongodb.morphia.annotations.*;

/**
 * @author lukasz
 */
@Entity("tickets")
@Indexes(
        {
                @Index(value = "from", fields = @Field("from")),
                @Index(value = "to", fields = @Field("to"))
        }
)
public class Ticket {
    @Id
    private String id;

    @Reference
    public User from;

    @Reference
    public User to;

    public String gift;

    public String note;

    public boolean active = true;

    public Ticket() {}

    public Ticket(User from, User to, String gift, String note) {
        this.from = from;
        this.to = to;
        this.gift = gift;
        this.note = note;
    }

}
