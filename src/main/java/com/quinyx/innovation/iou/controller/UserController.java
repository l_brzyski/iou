package com.quinyx.innovation.iou.controller;

import com.quinyx.innovation.iou.model.User;
import com.quinyx.innovation.iou.service.UserService;
import spark.Request;
import spark.Response;

import static com.quinyx.innovation.iou.JsonUtil.json;
import static spark.Spark.get;
import static spark.Spark.post;

/**
 * @author lukasz
 */
public class UserController {

    public UserController(final UserService userService) {

        get("/users", (Request request, Response response) -> userService.getAllUsers(), json());

        get("/users/:id", (Request request, Response response) -> {
            String id = request.params(":id");
            User user = userService.getUser(id);
            if (user != null) {
                return user;
            }
            response.status(400);
            return new ResponseError("No user with id '%s' found", id);
        }, json());

        post("/users", (Request request, Response response) -> userService.createUser(request.queryParams("email")), json());
    }

}
