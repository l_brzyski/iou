package com.quinyx.innovation.iou.controller;

import com.quinyx.innovation.iou.model.Ticket;
import com.quinyx.innovation.iou.model.User;
import com.quinyx.innovation.iou.service.TicketService;
import com.quinyx.innovation.iou.service.UserService;
import spark.Request;
import spark.Response;

import static com.quinyx.innovation.iou.JsonUtil.json;
import static spark.Spark.get;
import static spark.Spark.post;

/**
 * @author lukasz
 */
public class TicketController {

    public TicketController(final TicketService ticketService, UserService userService) {

        // get list of tickets
        get("/tickets", (Request request, Response response) -> ticketService.getTickets(userService.getUser(request.queryParams("from")), userService.getUser(request.queryParams("to"))), json());

        // create a new ticket
        post("/tickets", (Request request, Response response) -> {

                if (request.queryParams("from") == null || request.queryParams("from") == null || request.queryParams("gift") == null) {
                    response.status(400);
                    return new ResponseError("Missing parameters. Please make sure you specify 'from', 'to' and 'gift' parameters");
                }

                User from = userService.getUser(request.queryParams("from"));
                if (from == null) {
                    response.status(400);
                    return new ResponseError("No user with email '%s' found", request.queryParams("from"));
                }

                User to = userService.getUser(request.queryParams("to"));
                if (to == null) {
                    response.status(400);
                    return new ResponseError("No user with email '%s' found", request.queryParams("to"));
                }

                return ticketService.createTicket(from, to, request.queryParams("gift"), request.queryParams("note"));
            },
            json());

        // get ticket details
        get("/tickets/:id", (Request request, Response response) -> {
                String id = request.params(":id");
                Ticket ticket = ticketService.getTicket(id);
                if (ticket != null) {
                    return ticket;
                }
                response.status(400);
                return new ResponseError("No ticket with id '%s' found", id);
            },
            json());

        // inactivate ticket
        post("/tickets/*/inactivate", (Request request, Response response) -> {
                    String id = request.splat()[0];
                    Ticket ticket = ticketService.getTicket(id);
                    if (ticket != null) {
                        return ticketService.inactivateTicket(id);
                    }
                    response.status(400);
                    return new ResponseError("No ticket with id '%s' found", id);
            },
            json());
    }

}
